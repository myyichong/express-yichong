var express = require('express')
var router = express.Router()
var patModel = require('../models/patinfoModel')
router.post('/pat', (req, res) => {
    new patModel(req.body).save()
        .then((result) => {
            res.send({
                code: 200,
                msg: '发布成功',
                type: 1,
            })
        })
})

router.get('/pat', (req, res) => {
    patModel.find().then((result) => {
        res.send({
            code: 200,
            type: 1,
            data: result,
            msg: '发布成功'
        });
    })
});

router.get('/pat/area', (req, res) => {
    patModel.find().sort({
        "patarea": -1
    }).then((result) => {

        res.send({
            code: 200,
            type: 1,
            data: result,
            msg: '发布成功'
        });
    })
});
router.get('/pat/patprice', (req, res) => {
    patModel.find().sort({
        "patprice": -1
    }).then((result) => {
        res.send({
            code: 200,
            type: 1,
            data: result,
            msg: '发布成功'
        });
    })
});

router.delete('/pat', (req, res) => {
    let {
        _id
    } = req.query;
    patModel.remove({
            _id
        })
        .then((result) => {
            if (result) {
                res.send({
                    code: 200,
                    type: 1,
                    msg: '删除成功'
                })
            }
        }).catch((err) => {
            res.send({
                type: 0,
                code: 200,
                msg: '删除失败'
            })

        })
})
module.exports = router