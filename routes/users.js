var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var salt = bcrypt.genSaltSync(10); //盐，加密强度，值越大，密码越难破解


//引入数据库的表
let {
  User
} =require("../models/model")
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});


//register
router.post("/register",(req,res)=>{
    let body=req.body;
    // console.log(body);
    User.findOne({
      mobile:body.mobile
    }).then(result=>{
        if(result){
            res.json({
              code:200,
              type:0,
              msg:"用户名已经存在，注册失败"
            })
        }else{
          //查询数据库没有该号码，可以注册
          body.time=new Date();
          //密码加密
          var password = bcrypt.hashSync(body.password, salt);
          var confirmpwd = bcrypt.hashSync(body.confirmpwd, salt);
          console.log(password,confirmpwd)
          body.password=password
          body.confirmpwd=confirmpwd
          User.insertMany(body)
          .then(data=>{
              res.json({
                code:200,
                type:1,
                msg:"注册成功",
                data:data
              })
          })    
        }
    })
})





//login
//查询数据库中的手机号和密码，如果比对成功返回一个结果
router.post("/login",(req,res)=>{
  let body=req.body;
  // console.log(body);
  User.findOne({
    mobile:body.mobile,
  }).then(result=>{
    // console.log(result)
      if(result){//比对成功
        // console.log(result)
            let bool = bcrypt.compareSync(body.password,result.password)       
            if(bool){
              req.session.mobile=result.mobile//用于服务器存储登录成功后手机号
              console.log(req.session.mobile)
              res.json({
                  code:200,
                  type:1,
                  msg:"登录成功",
                  result,
                  mobile:req.session.mobile
              })
            }else{
              res.json({
                  code:200,
                  type:0,
                  msg:"密码错误",
              })
            }
          
      }else{
        res.json({
          code:200,
          type:0,
          msg:"该手机号还没注册"
      })
    }
  })
})


module.exports = router;
