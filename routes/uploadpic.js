var express = require('express');
var router = express.Router();
//上传头像需要的第三方模块
let multer=require("multer");
//引入数据库的表
let {
    User
  } =require("../models/model")
//固态硬盘存储DiskStorage
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './public/upload')//../
    },
    filename: function (req, file, cb) {
      cb(null, Date.now()+file.originalname)
    }
  })
  
  var upload = multer({ storage: storage }).any()//表示可以上传各种文件
  
  
  //上传头像
  router.post("/uploading",upload,(req,res)=>{
      console.log("文件上传成功")
      // console.log(req.files);
      if(req.files){
              let path=req.files[0].path;
              console.log(req.session)
              User.updateOne({mobile:req.session.mobile},{//登录成功后是一定能查到手机号
                $set:{
                  avatar:path
                }
              }).then(result=>{
                    res.json({
                        code:200,
                        type:1,
                        msg:"上传头像成功",
                        path,
                        result,
                        mobile:req.session.mobile
                    })           
                  })
            
                  
      }else{
          res.json({
            type:0,
            code:200,
            msg:"上传头像失败"
          })
      }
  })
  

  
  
//获取头像
router.post("/pic",(req,res)=>{
    if(req.session.mobile){
      User.findOne({
        mobile:req.session.mobile
      }).then(result=>{
          if(result.avatar){
              res.json({
                code:200,
                type:1,
                msg:"获取头像成功",
                result
              })
          }else{
              res.json({
                code:200,
                type:0,
                msg:"获取头像失败",
              })
          }
      })
    }
    
})

module.exports = router;