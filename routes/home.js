const express=require("express");
const router=express.Router();

let homeData=require("../models/home")

//admin上传用户信息
router.post("/pub",(req,res)=>{
    let body=req.body;
    console.log(body);
    if(req.session.mobile){
        homeData.insertMany(body)
        .then(data=>{
            res.send({
                code:200,
                type:1,
                msg:"信息发布成功"
            })
        })
    }else{
        res.send({
            code:200,
            type:-1,
            msg:"你还没有登录哦"
        })
    }
    
   
})



//主页获取用户信息
router.get("/homeinfo",(req,res)=>{
    homeData.find().then(homelist=>{
        res.json({
            code:200,
            type:1,
            homelist
        })
    }).catch(err=>{
        res.json({
            code:200,
            type:0,
            msg:"获取信息失败",
        })
    })
})

module.exports=router