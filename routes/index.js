var express = require('express');
var router = express.Router();
var stuModel = require('../models/stuModel');
/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Express'
  });
});

router.get('/test', function (req, res, next) {
  new stuModel({
    username: 'cysz11',
    age: '800',
    score: 102,
    isSleep: true
  }).save().then((res) => {
    console.log(res);
  })
});

router.get('/student', (req, res) => {
  stuModel.find().then((result) => {
    console.log(result);
    res.render('student.html', {
      result
    })

  })

});
module.exports = router;