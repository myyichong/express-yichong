var express = require('express');
var router = express.Router();



//引入数据库的表
let {
  User
} =require("../models/model")
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});



//admin获取用户注册的信息
router.get("/usermsg",(req,res)=>{

        User.find().then(result=>{
            res.send({
                code:200,
                type:1,
                msg:"获取用户注册信息成功",
                data:result
            })
        }).catch(err=>{
            res.send({
                code:200,
                type:0,
                msg:"获取用户注册信息失败",
            })
        })

    
})

//admin删除用户注册的信息
router.delete("/usermsg",(req,res)=>{
        let {_id} =req.query;
        User.remove({_id}).then(result=>{
            res.send({
                code:200,
                type:1,
                msg:"删除注册用户信息成功",
                data:result
            })
        }).catch(err=>{
            res.send({
                code:200,
                type:0,
                msg:"删除注册用户信息失败",
            })
        })
})




module.exports = router;
