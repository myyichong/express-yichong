const express=require("express");
const router=express.Router();

let timeData=require("../models/time");


//存储admin的预约时间管理
router.post("/appotime",(req,res)=>{
    let body=req.body;
    // console.log(body)
    timeData.insertMany(body).then(result=>{
            res.send({
                code:200,
                type:1,
                msg:"存储预约时间信息成功",
                data:result
            })
    }).catch(err=>{
        res.send({
            code:200,
            type:0,
            msg:"存储预约时间信息失败",
        })
    })
})

module.exports=router;