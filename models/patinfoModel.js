var mongoose = require('mongoose')

var Schema = mongoose.Schema;

var patSchema = new Schema({ //定义表结构

    imgurl: String,
    pattitle: String,
    patprice: Number,
    patarea: String,
    isgetting: Boolean,
    icon: String,
    user: String,
    patinfo: String

})

var patModel = mongoose.model('patModel', patSchema) //用以操作数据表结构对象的【数据模型对象】

module.exports = patModel