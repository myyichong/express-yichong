const mongoose = require('mongoose')
const Schema = mongoose.Schema
const actSchema = new Schema({
    img: String,
    shopname: String,
    address: String,
    order: Number,
    goodsay: Number,
    far: Number,
    usersay: String,
    act: String,
})

const actModel = mongoose.model('actModel', actSchema)

module.exports = actModel