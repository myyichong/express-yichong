var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var studentSchema = new Schema({
    username: String,
    age: Number,
    score: Number,
    isSleep: Boolean
});
var studentModel = mongoose.model('studentModel', studentSchema);

module.exports = studentModel;