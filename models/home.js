const mongoose=require("mongoose");

const Schema=mongoose.Schema;

const home_schema=new Schema({
    usertouxiang: String,
    username: String,
    time: String,
    price: String,
    desc:String,
    address:String,
    love:String,
    xiaoxi:String,
    isname:String,
    istime:String,
    imgs:Array
})

const homeData=mongoose.model("homeData",home_schema)
module.exports=homeData