const mongoose=require("mongoose");

const Schema=mongoose.Schema;

const time_schema=new Schema({
    choosetime:String,
    timelist:Array,
    alltime:Array
})

const timeData=mongoose.model("timeData",time_schema)
module.exports=timeData