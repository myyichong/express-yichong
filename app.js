////最新
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

//引入session插件
var session = require('express-session');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var homeRouter=require("./routes/home")
var timeRouter=require("./routes/time");
var patinfoRouter = require('./routes/patinfo');
var uploadRouter=require("./routes/uploadpic")
//引入cors
var cors=require("cors");

var app = express();

app.use(cors({
  origin:"http://localhost:8080",
  credentials:true
}))

//全局引入connetction
const connection=require("./db/connect")



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// session 中间件  必须写在路由中间件前面 
app.use(session({
  name:"AppTest",
  cookie:{maxAge:1000*60*60},  // session 时长
  secret:"test",
  resave:false,
  saveUninitialized:true
}))


app.use('/', indexRouter);
app.use('/admin',(req,res,next)=>{
  // console.log(req.session.mobile)//手机号
  if(req.session.mobile){//登录状态
        next()
  }else{//没有登录
      res.json({
        type:-1,
        code:200,
        msg:"你的登录状态已经失效了，赶紧重新登录吧"
      })
  }
}, patinfoRouter);
app.use('/users', usersRouter);
app.use('/uploadpic',(req,res,next)=>{
  console.log(req.session.mobile)//手机号
  if(req.session.mobile){//登录状态
        next()
  }else{//没有登录
      res.json({
        type:-1,
        code:200,
        msg:"你的登录状态已经失效了，赶紧重新登录吧"
      })
  }
},uploadRouter)
app.use('/home',homeRouter)
app.use('/adminmsg', (req,res,next)=>{
  console.log(req.session.mobile)//undefined
  if(req.session.mobile){//登录状态
        next()
  }else{//没有登录
      res.json({
        type:-1,
        code:200,
        msg:"你的登录状态已经失效了，赶紧重新登录吧"
      })
  }
},require("./routes/adminmsg"));
app.use('/time',(req,res,next)=>{
  console.log(req.session.mobile)
  if(req.session.mobile){//登录状态
      next()
  }else{//没有登录
    res.json({
      type:-1,
      code:200,
      msg:"你的登录状态已经失效了，赶紧重新登录吧"
    })
  }
},timeRouter)
app.use('/act',(req,res,next)=>{
  // console.log(req.session.mobile)
  if(req.session.mobile){//登录状态
      next()
  }else{//没有登录
    res.json({
      type:-1,
      code:200,
      msg:"你的登录状态已经失效了，赶紧重新登录吧"
    })
  }
}, require('./routes/act'));
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
